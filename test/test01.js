const webdriver = require("selenium-webdriver");
const By = require("selenium-webdriver").By;
const chrome = require("selenium-webdriver/chrome");
const assert = require("chai").assert;
const parallel = require("mocha.parallel");
const fs = require("fs");

const helper = require("../testHelper.js");

//describe("reminder function", function() {
//  this.timeout(30000);
//chaiのアサーションは以下を参照
//https://www.chaijs.com/api/assert/
parallel("test", function() {
  it("get title", done => {
    helper.execAsyncTest(async driver => {
      await driver.get("https://www.npmjs.com/package/selenium-webdriver");
      //タイトルチェック
      var title = await helper.getTitle(driver);
      assert.equal(title, "selenium-webdriver - npm");
    }, done);
  });
  it("get text", done => {
    helper.execAsyncTest(async driver => {
      //テストメソッド開始時にWebDriverが初期化されるので、以降の処理を書く
      //URLをセット
      await driver.get("https://www.npmjs.com/package/selenium-webdriver");
      //要素のテキストを取得
      var text = await driver
        .findElement(By.css(".package__packageName___30_rF"))
        .getText();
      //要素のテキストが正しいかチェック
      assert.equal(text, "selenium-webdriver");
    }, done);
  });
  it("get text2", done => {
    helper.execAsyncTest(async driver => {
      //テストメソッド開始時にWebDriverが初期化されるので、以降の処理を書く
      //URLをセット
      await driver.get("https://www.npmjs.com/package/selenium-webdriver");
      //要素のテキストを取得
      var text = await driver
        .findElement(By.css('a[href="#license"]'))
        .getAttribute("href");
      //要素のテキストが正しいかチェック
      assert.isString(text);
      assert.match(text, /#license$/);
    }, done);
  });
  it("get text3", done => {
    helper.execAsyncTest(async driver => {
      //テストメソッド開始時にWebDriverが初期化されるので、以降の処理を書く
      //URLをセット
      await driver.get("https://www.npmjs.com/package/selenium-webdriver");
      //要素のテキストを取得
      var text = await driver
        .findElement(By.css(".header__expansions___3REog"))
        .getText();
      //要素のテキストが正しいかチェック
      assert.isNotEmpty(text);
    }, done);
  });
  it("screenshot", done => {
    helper.execAsyncTest(async driver => {
      //URLをセット
      await driver.get("https://www.npmjs.com/package/");
      //スクショ
      helper.screenshot(driver, "data/" + Date.now() + ".png");
    }, done);
  });
});
