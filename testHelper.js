const webdriver = require("selenium-webdriver");
const By = require("selenium-webdriver").By;
const chrome = require("selenium-webdriver/chrome");
const assert = require("chai").assert;
const fs = require('fs');

//ブラウザの初期化
async function init() {
  driver = await new webdriver.Builder()
    .forBrowser("chrome")
    .setChromeOptions(new chrome.Options().headless()) //headless running
    .build();
  return driver;
}
module.exports.init = init;

//要素のテキストを取得
async function getElementText(driver, selector) {
  return await driver
    .findElement(By.css(selector))
    .getText()
    .then(text => {
      return text;
    });
}
module.exports.getElementText = getElementText;

//ドキュメントのタイトルを取得
function getTitle(driver) {
  return driver.getTitle().then(title => {
    return title;
  });
}
module.exports.getTitle = getTitle;

//非同期テストのガワ
function execAsyncTest(test, done) {
  (async () => {
    var driver = await init();
    try {
      await test(driver);
    } catch (ex) {
      throw ex;
    } finally {
      driver.quit();
    }
  })()
    .then(() => {
      done();
      return;
    })
    .catch((ex) => {
      console.log(ex);
      done(ex);
      return;
    });
}
module.exports.execAsyncTest = execAsyncTest;

module.exports.screenshot = async function(driver,filename){
  await driver.takeScreenshot().then((data)=>{
    fs.writeFile(
      filename,
      data.replace("^data:image/png;base64,",''), 'base64', function(error) {
        if(error) throw error;
      }
    );
  });
}