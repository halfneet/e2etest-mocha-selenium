# なにこれ

Selenium+mochaでE2Eテストやるやつ

# 必要なもの
- nodejs(検証してないけど新しめのやつ)
- Selenium WebDriver(https://www.npmjs.com/package/selenium-webdriver)
- 大量のメモリ

# 準備

```sh
npm install
```

# 動かし方

## コマンドラインから実行

```sh
mocha --timeout 30000
```

## VSCode
Mocha Test Explorer使うとよいです。

ファイル→基本設定→設定で「Mocha Explorer:Timeout」を変更しないとブラウザが開く前にタイムアウトで落ちます。
10秒ぐらいあればだいたい動くはず。

## 死んだHeadless Crhomeを停止(on winodws)

普通のChromeも死ぬ

```sh
taskkill /im chrome.exe /f
```